import unittest
from video_processing_library.speed_measurement import CountingPointDistanceSpeedMeasurement
from video_processing_library.rectangle_on_frame import RectangleOnFrame
from video_processing_library.object import Object

class TestObject(unittest.TestCase):
    _params = {'h': 12000, 'alpha': 30, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280,
               'height': 960}
    _rectangles = [RectangleOnFrame(610, 180, 60, 60, 450), RectangleOnFrame(580, 380, 120, 100, 465)]

    def test_object_getters_basic(self):
        object = Object(1)
        self.assertEqual(object.get_id(), 1)
        self.assertFalse(object.can_measure_speed(400, 30, CountingPointDistanceSpeedMeasurement))
        self.assertEqual(object.get_rectangles(), [])
        self.assertEqual(object.get_speed_measurements(), [])

    def test_can_measure_speed(self):
        fps = 30
        object = Object(1)
        self.assertFalse(object.can_measure_speed(400, fps, CountingPointDistanceSpeedMeasurement))
        object.add_rectangle_on_frame(RectangleOnFrame(610, 180, 60, 60, 450))
        object.add_rectangle_on_frame(RectangleOnFrame(580, 380, 120, 100, 465))
        self.assertTrue(object.can_measure_speed(465, fps, CountingPointDistanceSpeedMeasurement))

        speed_measurement = CountingPointDistanceSpeedMeasurement(1, object.get_id(), object.get_rectangles())
        speed_measurement.count_speed(self._params, fps)
        object.add_speed_measurement(speed_measurement)
        self.assertFalse(object.can_measure_speed(466, fps, CountingPointDistanceSpeedMeasurement))
        object.add_rectangle_on_frame(RectangleOnFrame(520, 760, 240, 180, 480))
        self.assertTrue(object.can_measure_speed(480, fps, CountingPointDistanceSpeedMeasurement))

    def test_raises_error_if_not_positive_fps(self):
        with self.assertRaises(ValueError):
            object = Object(1)
            object.can_measure_speed(466, 0, CountingPointDistanceSpeedMeasurement)

        with self.assertRaises(ValueError):
            object = Object(1)
            object.can_measure_speed(466, -30, CountingPointDistanceSpeedMeasurement)

if __name__ == '__main__':
    unittest.main()
