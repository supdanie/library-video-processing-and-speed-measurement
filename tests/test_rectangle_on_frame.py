import unittest
from video_processing_library.rectangle_on_frame import RectangleOnFrame


class TestRectangleOnFrame(unittest.TestCase):
    _rectangle = RectangleOnFrame(100, 150, 200, 250, 450)

    def test_rectangle(self):
        self.assertEqual(self._rectangle.x, 100)
        self.assertEqual(self._rectangle.y, 150)
        self.assertEqual(self._rectangle.width, 200)
        self.assertEqual(self._rectangle.height, 250)
        self.assertEqual(self._rectangle.frame_number, 450)
        self.assertEqual(self._rectangle.get_time_in_seconds(30), 15)
        self.assertEqual(self._rectangle.get_time_in_seconds(50), 9)

    def test_rectangle_with_invalid_values(self):
        with self.assertRaises(ValueError):
            RectangleOnFrame(-1, 2, 100, 100, 50)

        with self.assertRaises(ValueError):
            RectangleOnFrame(1, -2, 100, 100, 50)

        with self.assertRaises(ValueError):
            RectangleOnFrame(1, 2, 0, 100, 50)

        with self.assertRaises(ValueError):
            RectangleOnFrame(1, 2, 50, -1, 50)

        with self.assertRaises(ValueError):
            RectangleOnFrame(1, 2, 20, 100, -10)

    def test_raises_error_if_fps_not_positive(self):
        with self.assertRaises(ValueError):
            self._rectangle.get_time_in_seconds(0)

        with self.assertRaises(ValueError):
            self._rectangle.get_time_in_seconds(-30)

if __name__ == '__main__':
    unittest.main()
