import unittest
import os
import sys
from video_processing_library.video_processor import VideoProcessor
from video_processing_library.object_detector import BackgroundSubtractionObjectDetector
from video_processing_library.speed_measurement import CountingPointDistanceSpeedMeasurement
from video_processing_library.annotation import MediaFragmentAndWebVTTAnnotation

class TestVideoProcessing(unittest.TestCase):
    _params = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280,
               'height': 960, 'server_url': 'https://www.mujserver.cz/'}
    _video_processor = VideoProcessor()
    _current_file_path = os.path.dirname(os.path.realpath(__file__))
    _video_path = os.path.join(_current_file_path, 'video/video1.mp4')


    def test_processing_video(self):
        detected = 0
        annotation_generator = self._video_processor.process_video(str(self._video_path), self._params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
        for annotations in annotation_generator:
            self.assertIsNotNone(annotations)
            self.assertNotEqual(annotations, [])
            self.assertGreaterEqual(len(annotations), 2)
            self.assertTrue(annotations[0].startswith('https://www.mujserver.cz/'))
            detected += 1
        self.assertGreater(detected, 9)

    def test_raises_error_if_parameter_missing(self):
        params1 = {'h': 13000, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params2 = {'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params3 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params4 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params5 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'server_url': 'https://www.mujserver.cz'}
        params6 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}
        for params in [params1, params2, params3, params4, params5, params6]:
            with self.assertRaises(ValueError):
                annotation_generator = self._video_processor.process_video(str(self._video_path), params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
                for annotations in annotation_generator:
                    pass


    def test_raises_error_if_extra_parameter_given(self):
        params1 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'm': [5, 6], 'server_url': 'https://www.mujserver.cz'}
        params2 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'extra': 'value', 'server_url': 'https://www.mujserver.cz'}
        for params in [params1, params2]:
            with self.assertRaises(ValueError):
                annotation_generator = self._video_processor.process_video(str(self._video_path), params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
                for annotations in annotation_generator:
                    pass

    def test_raises_error_if_not_float_value(self):
        params1 = {'h': 13000, 'alpha': 45, 'f': 'focal length', 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params2 = {'h': 13000, 'alpha': '45 degrees', 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params3 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': '960 px', 'server_url': 'https://www.mujserver.cz'}
        for params in [params1, params2, params3]:
            with self.assertRaises(ValueError):
                annotation_generator = self._video_processor.process_video(str(self._video_path), params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
                for annotations in annotation_generator:
                    pass

    def test_raises_error_if_negative_or_zero_value(self):
        params1 = {'h': 13000, 'alpha': 45, 'f': 0, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params2 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 0, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params3 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 10, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 0, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params4 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': -20, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 4540, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params5 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 10, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': -4540, 'height': 960, 'server_url': 'https://www.mujserver.cz'}
        params6 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': -20, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 4540, 'height': -960, 'server_url': 'https://www.mujserver.cz'}
        params7 = {'h': 50, 'alpha': 45, 'f': 10, 'len_width': 20, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'x_c': -1, 'y_c': 440, 'server_url': 'https://www.mujserver.cz'}

        for params in [params1, params2, params3, params4, params5, params6, params7]:
            with self.assertRaises(ValueError):
                annotation_generator = self._video_processor.process_video(str(self._video_path), params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
                for annotations in annotation_generator:
                    pass

    def test_raises_error_if_distortion_coefficients_are_not_in_array(self):
        params1 = {'h': 14000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': '0, 1', 'server_url': 'https://www.mujserver.cz'}
        params2 = {'h': 14000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': 0.0001, 'server_url': 'https://www.mujserver.cz'}
        params3 = {'h': 14000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.000005], 'p': 0.00005, 'server_url': 'https://www.mujserver.cz'}
        params4 = {'h': 14000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': 0.0001, 'p': [0.00000004], 'server_url': 'https://www.mujserver.cz'}

        for params in [params1, params2, params3, params4]:
            with self.assertRaises(ValueError):
                annotation_generator = self._video_processor.process_video(str(self._video_path), params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
                for annotations in annotation_generator:
                    pass

if __name__ == '__main__':
    unittest.main()