import unittest
from video_processing_library.speed_measurement import CountingPointDistanceSpeedMeasurement
from video_processing_library.rectangle_on_frame import RectangleOnFrame

class TestSpeedMeasurement(unittest.TestCase):
    _params = {'h': 12000, 'alpha': 30, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280,
               'height': 960}
    _rectangles = [RectangleOnFrame(610, 180, 60, 60, 450), RectangleOnFrame(580, 380, 120, 100, 465)]

    def test_can_count_speed(self):
        speed_measurement = CountingPointDistanceSpeedMeasurement(1, 1, self._rectangles)
        self.assertEqual(speed_measurement.can_count_speed(20), True)
        self.assertEqual(speed_measurement.can_count_speed(30), True)
        self.assertEqual(speed_measurement.can_count_speed(50), False)

    def test_correct_speed_counted_and_then_returned(self):
        speed_measurement = CountingPointDistanceSpeedMeasurement(1, 1, self._rectangles)
        speed = speed_measurement.count_speed(self._params, 30)
        self.assertEqual(int(speed), 70)
        speed = speed_measurement.count_speed(self._params, 20)
        self.assertEqual(int(speed), 70)
        speed = speed_measurement.count_speed(self._params, 30)
        self.assertEqual(int(speed), 70)

    def test_correct_speed_counted_for_lower_fps(self):
        speed_measurement = CountingPointDistanceSpeedMeasurement(1, 1, self._rectangles)
        speed = speed_measurement.count_speed(self._params, 15)
        self.assertEqual(int(speed), 35)

    def test_can_count_speed_after_counting(self):
        fps = 30
        speed_measurement = CountingPointDistanceSpeedMeasurement(1, 1, self._rectangles)
        speed = speed_measurement.count_speed(self._params, fps)
        self.assertEqual(int(speed), 70)
        self.assertEqual(speed_measurement.can_count_speed_after_counting(466, fps), False)
        self.assertEqual(speed_measurement.can_count_speed_after_counting(479, fps), False)
        self.assertEqual(speed_measurement.can_count_speed_after_counting(480, fps), True)

    def test_getters_return_correct_values(self):
        speed_measurement = CountingPointDistanceSpeedMeasurement(1, 3, self._rectangles)
        self.assertEqual(speed_measurement.get_id(), 1)
        self.assertEqual(speed_measurement.get_object_id(), 3)
        self.assertEqual(speed_measurement.get_first_frame(), 0)
        self.assertEqual(speed_measurement.get_last_frame(), 0)
        speed = speed_measurement.count_speed(self._params, 30)
        self.assertEqual(int(speed), 70)
        self.assertEqual(speed_measurement.get_first_frame(), 450)
        self.assertEqual(speed_measurement.get_last_frame(), 465)

    def test_raises_error_if_not_positive_fps(self):
        with self.assertRaises(ValueError):
            speed_measurement = CountingPointDistanceSpeedMeasurement(1, 3, self._rectangles)
            speed_measurement.count_speed(self._params, 0)

        with self.assertRaises(ValueError):
            speed_measurement = CountingPointDistanceSpeedMeasurement(1, 3, self._rectangles)
            speed_measurement.count_speed(self._params, -50)

        with self.assertRaises(ValueError):
            speed_measurement = CountingPointDistanceSpeedMeasurement(1, 3, self._rectangles)
            speed_measurement.count_speed(self._params, 30)
            speed_measurement.can_count_speed_after_counting(466, 0)

        with self.assertRaises(ValueError):
            speed_measurement = CountingPointDistanceSpeedMeasurement(1, 3, self._rectangles)
            speed_measurement.count_speed(self._params, 30)
            speed_measurement.can_count_speed_after_counting(466, -30)

if __name__ == '__main__':
    unittest.main()
