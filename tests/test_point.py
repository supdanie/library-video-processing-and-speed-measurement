import unittest
from video_processing_library.point import Point

class TestPoint(unittest.TestCase):
    _params = {'h': 10000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}
    _params2 = {'h': 20000, 'alpha': 22.5, 'f': 5, 'len_width': 16, 'len_height': 12, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}

    def test_valid_real_coordinates(self):
        point = Point(640, 480)
        real_coordinates = point.count_real_coordinates(self._params)
        self.assertEqual(real_coordinates[0], 0)
        self.assertEqual(real_coordinates[1], 10000)
        self.assertEqual(real_coordinates[2], -10000)

    def test_count_real_distance(self):
        point = Point(640, 480)
        point2 = Point(640, 0)
        distance = point.count_distance(point2, self._params)
        self.assertEqual(int(distance), 8571)

    def test_return_infinity_coordinates(self):
        point = Point(640, 20)
        real_coordinates = point.count_real_coordinates(self._params2)
        self.assertEqual(real_coordinates, [float('inf'), float('inf'), -20000])

    def test_raises_error_if_negative_coordinates(self):
        with self.assertRaises(ValueError):
            Point(-5, 10)

        with self.assertRaises(ValueError):
            Point(5, -10)


if __name__ == '__main__':
    unittest.main()
