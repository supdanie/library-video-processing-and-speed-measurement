import unittest
from video_processing_library.rectangle_on_frame import RectangleOnFrame
from video_processing_library.speed_measurement import CountingPointDistanceSpeedMeasurement
from video_processing_library.annotation import MediaFragmentAndWebVTTAnnotation


class TestAnnotation(unittest.TestCase):
    _params = {'h': 12000, 'alpha': 30, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280,
               'height': 960}
    _rectangles = [RectangleOnFrame(610, 180, 60, 60, 450), RectangleOnFrame(580, 380, 120, 100, 465)]
    _speed_measurement = CountingPointDistanceSpeedMeasurement(3, 2, _rectangles)
    _annotation = MediaFragmentAndWebVTTAnnotation()

    def test_return_correct_annotations(self):
        self._speed_measurement.count_speed(self._params, 30)
        annotations = self._annotation.get_annotations_for_speed_measurement("http://localhost/video.mp4", self._speed_measurement, 30)
        self.assertEqual(len(annotations), 2)
        self.assertIn("http://localhost/video.mp4#id=3", annotations)
        self.assertIn("3\n00:15.000 --> 00:15.500\n{\"objectId\": \"2\", \"speed\": \"70\", \"mediaFragmentsURIs\": [\"http://localhost/video.mp4#t=15.0&xywh=610,180,60,60\", \"http://localhost/video.mp4#t=15.5&xywh=580,380,120,100\"]}", annotations)

    def test_throws_value_error_if_fps_not_positive(self):
        with self.assertRaises(ValueError):
            self._speed_measurement.count_speed(self._params, 0)

        with self.assertRaises(ValueError):
            self._speed_measurement.count_speed(self._params, -5)

        with self.assertRaises(ValueError):
            self._speed_measurement.count_speed(self._params, 10)
            self._annotation.get_annotations_for_speed_measurement("http://localhost/video.mp4", self._speed_measurement, 0)
        
        with self.assertRaises(ValueError):
            self._speed_measurement.count_speed(self._params, 10)
            self._annotation.get_annotations_for_speed_measurement("http://localhost/video.mp4", self._speed_measurement, -5)


if __name__ == '__main__':
    unittest.main()
