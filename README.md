# Library for video processing, speed measurement and generating annotations

This library is a part of my diploma thesis Application for processing videos to measure the speed of objects.

> Šup, Daniel. Aplikace pro zpracování videa za účelem měření rychlosti objektů. Diplomová práce. Praha: České vysoké učení technické v Praze. Fakulta informačních technologií, 2020.

## Instalation

First, you must copy the library to your computer from GIT repository. You can do it by the ```git clone``` command.

```
git clone git@gitlab.fit.cvut.cz:supdanie/library-video-processing-and-speed-measurement
```

or the `git clone` command with HTTPS URL.

```
git clone https://gitlab.fit.cvut.cz/supdanie/library-video-processing-and-speed-measurement
```

Then you must go to the directory library-video-processing-and-speed-measurement with the setup.py, library directory with source codes and tests with unit tests.

```
cd library-video-processing-and-speed-measurement
```

Then you must run setup.py to build the library.


```
python setup.py develop
```

When the library is built, you must install the library in your virtual environment by pip.
```
pip install video_processing_library
```

## Usage

The name of the main package and directory with source code is video_processing_library. If you want to import `VideoProcessor` class from `video_processor.py` file in the `video_processing_library` directory, you write this line at the top of your Python file at other imports.

```python
from video_processing_library.video_processor import VideoProcessor
```

The `VideoProcessor` class contains `process_video` instance method for processing video with given parameters, object of a class for object detector, class representing an algorithm for speed measurement and object of a class representing a way of generating annotations. You can call the method for processing video by this line

```python
VideoProcessor().process_video(video_path, params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
```
### Example of usage - printing of generated annotations

In the example there is shown a usage of the `VideoProcessor` class for processing a video. You can process the video by calling the method `process_video` of a created object of `VideoProcessor` class. You need to give the path to the video (relative or absolute), parameters of camera - h, f, width, height, len_width, len_height, p_x a p_y. Optional parameteres are alpha, beta, x_c, y_c, k and p. The parameters mean:


1. **h** -
Height of the camera above the ground plane in millimeters
2. **f** -
Focal distance in millimeters
3. **width**  -
Width of the frames in the video in pixels
4. **height**  -
Height of the frames in the video pixels
5. **len_width**  -
Width of the sensor in millimeters
6. **len_height**  -
Height of the sensor in millimeters
7. **p_x**  -
X coord of the principal point in pixels
8. **p_y**  -
Y coords of the principal point in pixels
9. **alpha**  -
The pitch angle of the camera in degrees
10. **beta**  -
The yaw angle of the camera in degrees
11. **x_c**  -
X coord of the distortion center of the camera in pixels
12. **y_c**  -
Y coord of the distortion center of the camera in pixels
13. **k**  -
Array of radial distortion coefficients
14. **p**  -
Array of tangential distortion coefficients

In the example the video is saved in the same directory as the source code and it has filename `my_video.mp4`. The video processor in this example uses background subtraction for object detection, counting distance between interesting points of the same objects for measuring speed of objects and combination of MediaFragments and WebVTT for annotating the video. The detection of moving objects by background subtraction and detecting contours in the foreground is ensured by object of `BackgroundSubtractorObjectDetector` class. The speed measurement is ensured by `CountingPointDistanceSpeedMeasurement` class. The generating Media Fragment URIs and WebVTT cues is ensured by an object of `MediaFragmentAndWebVTTAnnotation` class.

This example prints annotations of detected objects with counted speeds in my_video.mp4 videorecord. The processing of the video is called in the `process_video` function.

```
from video_processing_library.video_processor import VideoProcessor
from video_processing_library.background_subtraction_object_detection import BackgroundSubtractionObjectDetection
from video_processing_library.counting_point_speed_measurement import CountingPointSpeedMeasurement
from video_processing_library.media_fragment_and_webvtt_annotation import MediaFragmentAndWebVTTAnnotation

def process_video(video_path, params):
	video_processor = VideoProcessor()
	generator = video_processor.process_video(video_path, params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
	for annotations in generator:
    		print(annotations)

if __name__ == "__main__":
	video_path = "my_video.mp4"
	params = {'h': 13000, 'f': 8.2, 'width': 1280, 'height': 720, 'len_width': 13.2, 'len_height': 8.8, 'p_x': 640, 'p_y': 360, 'alpha': 45, 'beta': -5}
	process_video(video_path, params)

```



