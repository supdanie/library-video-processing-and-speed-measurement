from video_processing_library.video_processor import VideoProcessor
from video_processing_library.point import Point
from video_processing_library.object_detector import BackgroundSubtractionObjectDetector
from video_processing_library.speed_measurement import CountingPointDistanceSpeedMeasurement
from video_processing_library.annotation import MediaFragmentAndWebVTTAnnotation

video_processor = VideoProcessor()
point_1 = Point(350, 340)
point_2 = Point(430, 700)
params = {'h': 13000, 'f': 8.2, 'width': 1280, 'height': 720, 'len_width': 13.2, 'len_height': 8.8, 'p_x': 640, 'p_y': 360, 'alpha': 45, 'beta': -5, 'server_url': 'https://www.mujserver.cz'}
generator = video_processor.process_video('/home/user/zaznamprodiplomku.mp4', params, BackgroundSubtractionObjectDetector(), CountingPointDistanceSpeedMeasurement, MediaFragmentAndWebVTTAnnotation())
for annotations in generator:
    print(annotations)