from video_processing_library.speed_measurement import SpeedMeasurement

class Annotation:
    """
    This class is a parent class for all classes which represent a way of annotation of a video. Descendants of this class ensure adding metadata about position, size on the frame, time and speed to the video.
    """
    def get_annotations_for_speed_measurement(self, video_url, speed_measurement, fps) -> list:
        """
        This method generates annotations for the given speed measurement of the object. The generated annotations contains the sizes and the positions from the list of objects of the RectangleOnFrameClass and the counted speed.

        :param video_url: The whole URL with path to the video
        :param speed_measurement: An object of SpeedMeasurement class which represents the speed measurement of the object in the given time range
        :param fps: Frame rate
        :return: Array with annotations with metadata about the given speed measurement such as speed, positions and sizes on the frames and times.
        """
        return []

class MediaFragmentAndWebVTTAnnotation(Annotation):
    """
    This class is a child class of the Annotation class which represents the combination of Media Fragment URI and WebVTT for adding metadata about time, position and speed. It ensures generating MediaFragment URIs for each saved rectangle on a frame represented by an object of RectangleOnFrame class and WebVTT fragment for each speed measurement.
    """
    def get_annotations_for_speed_measurement(self, video_url, speed_measurement, fps) -> list:
        """
        This method generates Media Fragment URIs for each saved rectangle on a frame saved in the object representing the speed measurement with moving object and WebVTT cue for the given speed measurement of the object.

        :param video_url: The whole URL with path to the video
        :param speed_measurement: An object of SpeedMeasurement class which represents the speed measurement of the object in the given time range
        :param fps: Frame rate of the processed video
        :return: Array of generated annotations containing MediaFragment URIs for each rectangle on frame saved in the object representing the speed measurement and WebVTT cue for the given speed measurement.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")

        annotations = []
        media_fragment_URI = self.__get_media_fragments_URI(video_url, speed_measurement)
        annotations.append(media_fragment_URI)
        webVTT_fragment = self.__get_webVTT_fragment(video_url, speed_measurement, fps)
        annotations.append(webVTT_fragment)
        return annotations

    def __get_webVTT_fragment(self, video_url: str, speed_measurement: SpeedMeasurement, fps: int) -> str:
        """
        This method returns a WebVTT cue for the given speed measurement consisting of three lines. On the first line there are the identificator of the speed measurement and the object identificator. On the second line there is the time range with start and end time in the format minutes:seconds.hundredths. The third line contains the counted speed of the object.

        :param speed_measurement: An object of SpeedMeasurement class which represents the speed measurement of the object in the given time range
        :param fps: Frame rate of the processed video
        :return: The WebVTT cue with the counted speed, identificator of the object whose speed is counted and the time range of the speed measurement.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")

        first_line = str(speed_measurement.get_id()) + "\n"
        first_time = self.__get_string_with_time_for_webVTT(speed_measurement.get_first_frame(), fps)
        second_time = self.__get_string_with_time_for_webVTT(speed_measurement.get_last_frame(), fps)
        second_line = first_time + " --> " + second_time + "\n"
        third_line = '{"objectId": "' + str(speed_measurement.get_object_id()) + '", '
        third_line += '"speed": "' + str(int(speed_measurement.count_speed({}, fps))) + '", '
        third_line += '"mediaFragmentsURIs": ['
        comma = ''
        for rectangle_on_frame in speed_measurement.rectangles_on_frame:
            if rectangle_on_frame.frame_number < speed_measurement.get_first_frame():
                continue
            time_fragment = 't=' + str(rectangle_on_frame.get_time_in_seconds(fps))
            x = rectangle_on_frame.x
            y = rectangle_on_frame.y
            w = rectangle_on_frame.width
            h = rectangle_on_frame.height
            xywh_fragment = 'xywh=' + str(x) + ',' + str(y) + ',' + str(w) + ',' + str(h)
            media_fragments_uri = video_url + '#' + time_fragment + '&' + xywh_fragment
            third_line += comma
            third_line += '"' + media_fragments_uri + '"'
            comma = ', '
        third_line += ']}'
        return first_line + second_line + third_line

    def __get_media_fragments_URI(self, video_url: str, speed_measurement: SpeedMeasurement) -> str:
        """
        This method returns a Media Fragments URI with the identificator of the speed measurement

        :param video_url: The whole URL with path to the video
        :param speed_measurement: An object of SpeedMeasurement class which represents the speed measurement of a moving object
        :return: The Media Fragments URI fro the given speed measurement.
        """
        id_fragment = "id=" + str(speed_measurement.get_id())
        return video_url + '#' + id_fragment

    def __get_string_with_time_for_webVTT(self, frame_number: int, fps: int) -> str:
        """
        This method gets and returns a string with a time when the given frame will be played in format minutes:seconds.hundredths

        :param frame_number: The number representing the order of the frame.
        :param fps: Frame rate of the processed video
        :return: String with a time when the frame with the given order will be played in format minutes:seconds.hundredths
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")
        if frame_number < 0:
            raise ValueError("Frame number must not be negative.")

        seconds = float(frame_number) / float(fps)
        minutes = 0
        if seconds >= 60:
            minutes = int(seconds / 60)
            seconds = seconds - minutes * 60
        second_string = "{:.3f}".format(seconds)
        return str(minutes).zfill(2) + ":" + second_string.zfill(6)