from video_processing_library.object import Object
from video_processing_library.object_detector import ObjectDetector
from video_processing_library.annotation import Annotation
import cv2


class VideoProcessor:
    """
    This class processes the video and it is called from a REST API to process the video.
    """
    def process_video(self, video, params, object_detector: ObjectDetector, speed_measurement_class, annotation: Annotation) -> list:
        """
        This method proccesses the video, ensured the object detection, speed measurement and it yields an array of annotations when speed of any moving object is being measured.

        :param video: Relative or absolute path to the video
        :param params: Map with the parameters important for the speed measurement
        :param object_detector: Detector of the objects for getting the list of rectangles with moving objects.
        :param speed_measurement_class: Class which represents the selected algorithm of speed measurement.
        :param annotation: Object which ensures generating annotations after measuring of the speed of an object.
        :return: It yields the annotations generated after every speed measurement.
        """
        if not speed_measurement_class.validate_params(params):
            raise ValueError("Invalid parameters")

        video_capture = cv2.VideoCapture(video)
        fps = video_capture.get(cv2.CAP_PROP_FPS)
        params['width'] = video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)
        params['height'] = video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
        params['p_x'] = params['p_x'] if 'p_x' in params else params['width'] / 2
        params['p_y'] = params['p_y'] if 'p_y' in params else params['height'] / 2
        frame_number = 0
        measurement_id = 1
        object_id = 1
        objects_in_video = []
        while video_capture.isOpened():
            ret, frame = video_capture.read()
            if ret:
                bounding_rectangles = object_detector.detect_objects(frame, frame_number)
                for rectangle in bounding_rectangles:
                    object_for_adding = None
                    for object_in_video in objects_in_video:
                        last_rectangle = object_in_video.get_rectangles()[-1]
                        if last_rectangle.frame_number < frame_number - 1:
                            continue
                        if (abs(last_rectangle.x - rectangle.x) < last_rectangle.width / 4 and
                           abs(last_rectangle.y - rectangle.y) < last_rectangle.height / 4 and
                           rectangle.frame_number - last_rectangle.frame_number == 1):
                            object_for_adding = object_in_video
                            break
                    if object_for_adding is None:
                        object_for_adding = Object(object_id)
                        objects_in_video.append(object_for_adding)
                        object_id += 1
                    object_for_adding.add_rectangle_on_frame(rectangle)
                    object_in_video = object_for_adding
                    if object_in_video.can_measure_speed(frame_number, fps, speed_measurement_class):
                        speed_measurement = speed_measurement_class(measurement_id, object_in_video.get_id(), object_in_video.get_rectangles())
                        speed = speed_measurement.count_speed(params, fps)
                        object_in_video.add_speed_measurement(speed_measurement)
                        measurement_id += 1
                        video_file = video.split('/')[-1]
                        annotations_for_speed_measurement = annotation.get_annotations_for_speed_measurement(params['server_url'] + video_file, speed_measurement, fps)
                        yield annotations_for_speed_measurement
                frame_number += 1
                if cv2.waitKey(1) & 0xff==ord('q'):
                    break

            else:
                break
        video_capture.release()
        cv2.destroyAllWindows()

