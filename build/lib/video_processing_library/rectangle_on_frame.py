class RectangleOnFrame:
    """
    This class represents a rectangle on a frame bounding a contour a moving object. It is used for saving information about the positions and sizes of objects on different frames.
    """
    def __init__(self, x: int, y: int, width: int, height: int, frame_number: int):
        """
        :param x: The x coordinate of the rectangle bounding a contour of moving object.
        :param y: The y coordinate of the rectangle bounding a contour of moving object.
        :param width: The width of the rectangle bounding a contour of moving object.
        :param height: The height of the rectangle bounding a contour of moving object.
        :param frame_number: The order of the frame on which the object can be bounded by this rectangle.
        """
        if x < 0 or y < 0 or width <= 0 or height <= 0 or frame_number < 0:
            raise ValueError("Invalid coords, dimensions or frame number")

        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.frame_number = frame_number

    def get_time_in_seconds(self, fps: int):
        """
        This method converts the given order of the frame to real time in seconds.

        :param fps: The frame rate of the processed video.
        :return: The real time in seconds when the frame, on which is the object inside the given rectangle, is played.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")

        return float(self.frame_number) / float(fps)