from video_processing_library.speed_measurement import SpeedMeasurement
from video_processing_library.rectangle_on_frame import RectangleOnFrame

class Object:
    """
    This class represents a moving object whose speed can be measured once or several times.
    """

    def __init__(self, id):
        """
        :param id: The unique identificator of the object.
        """
        self._id = id
        self._speed_measurements = []
        self._rectangles_on_frames = []

    def get_id(self):
        """
        :return: The identificator of the object.
        """
        return self._id

    def add_rectangle_on_frame(self, rectangle_on_frame: RectangleOnFrame):
        """
        This method adds an information about the position and size of the object on the frame with the given order. All the information are saved in the instance of the RectangleOnFrame class.

        :param rectangle_on_frame: The instance of RectangleOnFrame class with information about the position and size of the bounding rectangle of the contour of the object
        """
        self._rectangles_on_frames.append(rectangle_on_frame)

    def can_measure_speed(self, frame_number, fps, speed_measurement_class: SpeedMeasurement):
        """
        This method finds out whether speed of the object can be measured and there are enough saved information about positions and sizes of the moving object.

        :param frame_number: The order of the actual frame
        :param fps: Frame rate of the processed video
        :param speed_measurement_class:
        :return: Boolean representing whether the speed of the object can be counted.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")
        if frame_number < 0:
            raise ValueError("Frame number must not be negative.")

        speed_measurement = speed_measurement_class(0, self._id, self._rectangles_on_frames)
        if not speed_measurement.can_count_speed(fps):
            return False
        for speed_measurement in self._speed_measurements:
            if not speed_measurement.can_count_speed_after_counting(frame_number, fps):
                return False
        return True

    def get_rectangles(self):
        """
        :return: The array of objects with information about positions and sizes of the moving objects on frames.
        """
        return self._rectangles_on_frames

    def add_speed_measurement(self, speed_measurement: SpeedMeasurement):
        """
        This methods adds information about the actual speed measurement of the moving object.

        :param speed_measurement: The object representing the actual speed measurement of the object.
        """
        self._speed_measurements.append(speed_measurement)

    def get_speed_measurements(self):
        """
        :return: The array of objects representing a speed measurement of the moving object.
        """
        return self._speed_measurements
