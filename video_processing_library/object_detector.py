from video_processing_library.rectangle_on_frame import RectangleOnFrame
import cv2
import numpy as np

class ObjectDetector:
    """
    This class represents object detection by any algorithm. It is a parent class of all classes which detect object by an algorithm.
    """
    def detect_objects(self, frame, frame_number: int) -> list:
        """
        This method detects objects in the given frame with the given order. There the method returns an empty array, because it is a dummy implementation. It is an abstract method.

        :param frame: The representation of the frame
        :param frame_number: The order of the given frame
        :return: Empty array, because it is an abstract method.
        """
        return []

class BackgroundSubtractionObjectDetector(ObjectDetector):
    """
    This class represents the object detection by subtracting background and finding contours in the foreground. It ensures detection of the moving objects from the foreground and finding the bounding rectangles.
    """

    def __init__(self):
        self.__background_subtractor = cv2.createBackgroundSubtractorMOG2(detectShadows=False)
        self.__kernal_operator_open = np.ones((4, 4), np.uint8)
        self.__kernal_operator_close = np.ones((5, 5), np.uint8)

    def detect_objects(self, frame, frame_number: int) -> list:
        """
        This method detects objects by subtracting background. It finds contours of moving objects in the foreground. For each found contour it finds a bounding rectangle and the bounding rectangle and the order of the actual frame are saved to an object of RectangleOnFrame class.

        :param frame: The representation of the frame
        :param frame_number: The order of the frame in the video
        :return: The list of rectangles on the actual frame which bounds any contour of a moving object.
        """
        if frame_number < 0:
            raise ValueError("Frame number must not be negative.")

        fgmask = self.__background_subtractor.apply(frame)
        ret, imBin = cv2.threshold(fgmask, 200, 255, cv2.THRESH_BINARY)
        mask = cv2.morphologyEx(imBin, cv2.MORPH_OPEN, self.__kernal_operator_open)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, self.__kernal_operator_close)
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        objects = []
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            if w > 50 and h > 50:
                objects.append(RectangleOnFrame(x, y, w, h, frame_number))
        return objects