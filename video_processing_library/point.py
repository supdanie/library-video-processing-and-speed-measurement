import math

class Point:
    """
    This class represents any point on the frame. This class can represent an interesting point of any moving object.
    """
    def __init__(self, x: int, y: int):
        """
        :param x: The x coordinate of the point.
        :param y: The y coordinate of the point.
        """
        if x < 0 or y < 0:
            raise ValueError("Invalid coordinates of the point.")
        self.x = x
        self.y = y
    
    def count_real_coordinates(self, params):
        """
        This method counts the real coordinates in millimeters from the coordinates of the point in the frame with the given size. It finds and returns the point on the ground plane which is shown at the given frame coordinates. It can also consider radial and tangential distrotion of the camera.

        :param params: The map with the parameters of the camera - focal distance, len dimensions, pitch angle and yaw angle, width and height of frames in pixels or camera distortion coefficients.
        :return: Array with real coordinates in millimeters of the point in the frame.
        """
        coordinates_after_distortion_correction = self._count_coordinates_after_distortion_correction(params)
        x = coordinates_after_distortion_correction[0]
        y = coordinates_after_distortion_correction[1]
        alpha = math.radians(params['alpha']) if 'alpha' in params else 0.0
        beta = math.radians(params['beta']) if 'beta' in params else 0.0
        d_x = x - params['p_x']
        d_y = - (y - params['p_y'])
        real_d_x = d_x / (params['width'] / params['len_width'])
        real_d_y = d_y / (params['height'] / params['len_height'])
        alpha_2 = alpha - math.atan(real_d_y / params['f'])
        if alpha_2 <= 1e-6:
            return [float('inf'), float('inf'), -params['h']]
        z_t = math.sqrt(params['f'] ** 2 + real_d_y ** 2) * math.sin(alpha_2)
        y_t = math.sqrt(params['f'] ** 2 + real_d_y ** 2) * math.cos(alpha_2)
        x_t = real_d_x
        coef = params['h'] / z_t
        x_tt = math.cos(beta) * x_t + math.sin(beta) * y_t
        y_tt = math.cos(beta) * y_t - math.sin(beta) * x_t
        real_x = x_tt * coef
        real_y = y_tt * coef
        return [real_x, real_y, -params['h']]
    
    def count_distance(self, second_point, params):
        """
        This method counts distance from the point to the given point. It finds the real coordinates of both points and then counts the real distance in milimeters.

        :param second_point: The second point which the distance from the point is counted to
        :param params: The map with the parameters of the camera - focal distance, len dimensions, pitch angle and yaw angle, width and height of frames in pixels or camera distortion coefficients.
        :return: The real distance between the point and the given second point in millimeters.
        """
        this_point_real_coordinates = self.count_real_coordinates(params)
        second_point_real_coordinates = second_point.count_real_coordinates(params)
        return math.sqrt((this_point_real_coordinates[0] - second_point_real_coordinates[0]) ** 2 + (this_point_real_coordinates[1] - second_point_real_coordinates[1]) ** 2)

    def _count_coordinates_after_distortion_correction(self, params):
        """
        This function counts the x' and y' coordinates after radial and tangential distortion correction. It counts the radial and tangential distortion and corrected coords.

        :param params: The map with the parameters of the camera with distortion coefficients.
        :return: x' and y' coordinates corrected after radial and tangential distortion correction
        """
        distortion_center_x = params['x_c'] if 'x_c' in params else params['p_x']
        distortion_center_y = params['y_c'] if 'y_c' in params else params['p_y']
        r = math.sqrt((self.x - distortion_center_x) ** 2 + (self.y - distortion_center_y) ** 2)

        radial_distortion_coefficients = params['k'] if 'k' in params else []
        radial_distortion_polynome = 0
        degree = 0
        for k in radial_distortion_coefficients:
            degree += 2
            radial_distortion_polynome += k * (r ** degree)
        x_t = self.x + (self.x - distortion_center_x)*radial_distortion_polynome
        y_t = self.y + (self.y - distortion_center_y)*radial_distortion_polynome

        tangential_distortion_coefficients = params['p'] if 'p' in params else [0, 0]
        p0 = tangential_distortion_coefficients[0]
        p1 = tangential_distortion_coefficients[1]
        tangential_distortion_polynome_x = p0 * (r ** 2 + 2 * (self.x - distortion_center_x) ** 2) + 2 * p1 * (self.x - distortion_center_x) * (self.y - distortion_center_y)
        tangential_distortion_polynome_y = 2 * p0 * (self.x - distortion_center_x) * (self.y - distortion_center_y) + p1 * (r ** 2 + 2 * (self.y - distortion_center_y) ** 2)
        polynome_with_next_coefficients = 1
        degree = 0
        for p in tangential_distortion_coefficients[2:]:
            degree += 2
            polynome_with_next_coefficients = p * (r ** degree)
        tangential_distortion_polynome_x *= polynome_with_next_coefficients
        tangential_distortion_polynome_y *= polynome_with_next_coefficients
        x_t += tangential_distortion_polynome_x
        y_t += tangential_distortion_polynome_y
        return [x_t, y_t]

