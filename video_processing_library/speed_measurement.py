from video_processing_library.rectangle_on_frame import RectangleOnFrame
from video_processing_library.point import Point

class SpeedMeasurement:
    """
    This class represents a speed measurement. It is a parent class of all classes which represent an algorithm of counting speed of the objects on a video.
    """
    def __init__(self, id, object_id, rectangles_on_frame):
        """
        :param id: The identificator of the speed measurement
        :param object_id: The identificator of the object
        :param rectangles_on_frame: The list of rectangles on frame used for counting the speed of the object with the given identificator.
        """
        if not isinstance(rectangles_on_frame, list):
            raise ValueError("Rectangles_on_frame is not a list.")
        for rectangle_on_frame in rectangles_on_frame:
            if not isinstance(rectangle_on_frame, RectangleOnFrame):
                raise ValueError("Rectangles_on_frame must contain only rectangles.")

        self._id = id
        self._object_id = object_id
        self._first_frame = 0
        self._last_frame = 0
        self._counted_speed = None
        self.rectangles_on_frame = rectangles_on_frame

    def count_speed(self, params, fps):
        """
        This method ensures counting the speed of the object by any algorithm. This dummy implementation always return zero, because this method is abstract.

        :param params: The map with the parameters important for the speed measurement
        :param fps: Frame rate of the processed video.
        :return: Zero, because it is a dummy implementation
        """
        return 0

    def can_count_speed_after_counting(self, frame_number, fps):
        """
        This method returns whether the speed of the object can be measured again when the number of the actual frame is given. This dummy implementation always returns False, because this method is abstract.

        :param frame_number: The order of the actual frame when the speed of the object may be counted.
        :param fps: Frame rate of the processed video.
        :return: False, because it is a dummy implementation.
        """
        return False

    def can_count_speed(self, fps):
        """
        This method returns whether the speed can be counted and whether there is enough saved information about the positions and sizes of the object on different frames. This dummy implementation always returns False, because this method is abstract.

        :param fps: Frame rate of the processed video.
        :return: False, because it is a dummy implementation.
        """
        return False

    def get_id(self):
        """
        :return: The identificator of the speed measurement represented by the object of this class.
        """
        return self._id

    def get_object_id(self):
        """
        :return: The identificator of the object whose speed is measured.
        """
        return self._object_id

    def get_first_frame(self):
        """
        :return: The order of the first frame of the last time range for which the speed was measured.
        """
        return self._first_frame

    def get_last_frame(self):
        """
        :return: The order of the last frame of the last time range for which the speed was measured.
        """
        return self._last_frame

    @staticmethod
    def validate_params(params):
        """
        This method checks whether there were given all necessary params and whether they have suitable values.

        :param params: Hash with the given parameters.
        :return: Whether the hash with the given parameters is valid or not.
        """
        return True

class CountingPointDistanceSpeedMeasurement(SpeedMeasurement):
    """
    This class represents the algorithm of counting speed by counting the real coordinates of the detected interesting point of the rectangles on a frame and following counting the real distance between the two interesting points of the same object on different frames with the given parameters.
    """
    TIME_RANGE_FOR_SPEED_MEASURE = 0.5
    """
    This constant represents how often is speed of an object measured. The value 0.5 means that the speed of an object is measured each 0.5 second (twice a second).
    """

    def count_speed(self, params, fps: int):
        """
        This method counts the speed of the objects by counting the real distance which the object travelled in the given time range. It counts the real distance between the two interesting points from different rectangles on a frame with the same moving object and then counts the speed. The counted speed and the time range for which the speed is counted are saved.

        :param params: The map with the parameters of the camera - focal distance, len dimensions, pitch angle and yaw angle, width and height of frames in pixels or camera distortion coefficients.
        :param fps: Frame rate of the processed video.
        :return: The counted speed if the speed is not counted or the last counted speed saved in the class variable.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")

        if self._counted_speed is not None:
            return self._counted_speed
        rectangles = self.__get_rectangles_for_speed_measure(fps)
        if rectangles[0] is None:
            return False

        first_interesting_point = self.__get_interesting_point(rectangles[0])
        second_interesting_point = self.__get_interesting_point(rectangles[1])
        distance = first_interesting_point.count_distance(second_interesting_point, params)
        frames_back = rectangles[1].frame_number - rectangles[0].frame_number
        speed = distance * 3.6 * fps / frames_back / 1000
        self._counted_speed = speed
        self._first_frame = rectangles[0].frame_number
        self._last_frame = rectangles[1].frame_number
        return speed

    def can_count_speed_after_counting(self, frame_number: int, fps: int):
        """
        This method finds whether the speed of the object can be counted again when the frame with the given order is played. It compares the time of last speed measurement with the current time and finds out whether it was passed at least 0.5 second from last speed measurement of the object.

        :param frame_number: The order of the actual frame.
        :param fps: Frame rate of the processed video.
        :return: Boolean representing whether the speed can be counted after a previous speed measurement now or not.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")
        if frame_number < 0:
            raise ValueError("Frame number must not be negative.")

        if frame_number < self._last_frame:
            return False
        return frame_number >= self._last_frame + round(self.TIME_RANGE_FOR_SPEED_MEASURE * float(fps))

    def can_count_speed(self, fps: int):
        """
        This method finds out whether the speed of the moving object can be counted and whether it passed at least 0.5 second from the time the object was first detected.

        :param fps: Frame rate of the processed video.
        :return: Boolean representing whether it passed at least 0.5 second from the time the object was first detected.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")

        rectangles = self.__get_rectangles_for_speed_measure(fps)
        if rectangles[0] is None:
            return False
        first_frame = rectangles[0].frame_number
        last_frame = rectangles[1].frame_number
        return last_frame - first_frame >= round(self.TIME_RANGE_FOR_SPEED_MEASURE * float(fps))

    def __get_interesting_point(self, rectangle_on_frame: RectangleOnFrame) -> Point:
        """
        This method returns an interesting point at the bottom at the center of the given rectangle.

        :param rectangle_on_frame: The rectangle on the actual frame with position and size.
        :return: Interesting point at the bottom at the center of the given rectangle.
        """
        return Point(rectangle_on_frame.x + rectangle_on_frame.width / 2, rectangle_on_frame.y + rectangle_on_frame.height)

    def __get_rectangles_for_speed_measure(self, fps: int) -> list:
        """
        This method finds and returns rectangle on the last frame and rectangle on the frame 0.5 second ago. If the object was not detected, it returns an array with two None elements. If the object was detected first later than 0.5 second ago, it returns an array with None and rectangle on the last frame.

        :param fps: Frame rate of the processed video.
        :return: Array with None elements or rectangle on the last frame and rectangle on the frame 0.5 second ago.
        """
        if fps <= 0:
            raise ValueError("Frame rate must be positive.")

        first_rectangle = None
        last_rectangle = None
        for rectangle_on_frame in reversed(self.rectangles_on_frame):
            if last_rectangle is None:
                last_rectangle = rectangle_on_frame
            first_rectangle = rectangle_on_frame
            if last_rectangle.frame_number - first_rectangle.frame_number >= round(self.TIME_RANGE_FOR_SPEED_MEASURE * float(fps)):
                break
        return [first_rectangle, last_rectangle]

    @staticmethod
    def validate_params(params):
        """
        This method checks whether there were given all necessary params and whether they have suitable values.

        :param params: Hash with the given parameters.
        :return: Whether the hash with the given parameters is valid or not.
        """
        required_params = ['h', 'f', 'alpha', 'len_width', 'len_height', 'width', 'height', 'p_x', 'p_y', 'server_url']
        for required_param in required_params:
            if required_param not in params:
                return False
            if not isfloat(params[required_param]) and required_param != 'server_url':
                return False
            if required_param not in ['alpha', 'server_url'] and float(params[required_param]) <= 0:
                return False

        allowed_params = ['x_c', 'y_c', 'beta', 'k', 'p']
        for param in params:
            if param not in required_params and param not in allowed_params:
                return False
            if param in ['x_c', 'y_c'] and (not isfloat(params[param]) or float(params[param]) < 0):
                return False
            if param in ['k', 'p'] and not isinstance(params[param], list):
                return False
            if param in ['k', 'p']:
                for element in params[param]:
                    if not isfloat(element):
                        return False

        return True


def isfloat(str):
    try:
        float(str)
        return True
    except ValueError:
        return False