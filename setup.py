from setuptools import find_packages, setup
import video_processing_library
from video_processing_library import *

setup(
	name='video_processing_library',
	version='1.0.0',
	author='Daniel Sup',
	author_email='supdanie@fit.cvut.cz',
	packages=find_packages(),
	install_requires = [
		'numpy==1.18.2',
		'opencv-python==4.2.0.32'
	]
)
